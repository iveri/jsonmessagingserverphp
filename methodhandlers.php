<?php

require_once 'methods.php';

function RegisterUser($requestJSON)
{
    $responseJSONString = file_get_contents("./responseJSONs/".Methods::RegisterUserResult.".json");
    
    $responseJSON = json_decode($responseJSONString);

    $responseJSON->messageId = $requestJSON->messageId;

    return $responseJSON;
}

function AuthorizeUser($requestJSON)
{
    $responseJSONString = file_get_contents("./responseJSONs/".Methods::AuthorizeUserResult.".json");
    
    $responseJSON = json_decode($responseJSONString);

    $responseJSON->messageId = $requestJSON->messageId;

    return $responseJSON;
}